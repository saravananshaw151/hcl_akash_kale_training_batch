package abc;
interface A {
		void dis1();
}
class B implements A {
		public void dis1() {
			System.out.println("dis1 method override by B class");
		}
}
public class AnonymousInnerClassDemo {
	public static void main(String[] args) {
		//1st Option 
		A obj1 = new B();
		obj1.dis1();
		// 2nd Option 
		A obj2 = new A() {						// anonymous inner class 
			public void dis1() {
				System.out.println("dis2 method override by anonymous class with 1st way");
			}
		};
		obj2.dis1();
		A obj3 = new A() {						// anonymous inner class 
			public void dis1() {
				System.out.println("dis2 method override by anonymous class with 2nd way");
			}
		};
		obj3.dis1();
		//3rd Option 
		
		A obj4 = ()->System.out.println("This is simple lambda expression");
		obj4.dis1();
		
	}

}




