package com;

import java.util.function.Consumer;
class MyConsumer implements Consumer<String>{
	@Override
	public void accept(String t) {
		System.out.println("Value is "+t);
	}
	
}
public class PreDefinedFunctionaInterface {
	public static void main(String[] args) {
		//1st way - old way 
		Consumer<String> cc = new MyConsumer();			// run time polymorphism 
		cc.accept("Raj");
		
		// 2nd way - using lambda expression 
		Consumer<String > cc1 = (name)->System.out.println("Value is using lambda with cc1"+name);
		cc1.accept("Ravi");
		
		Consumer<String > cc2 = (name)->System.out.println("Value is using lambda with cc2"+name);
		cc2.accept("Ravi");
		
	}

}
