package com;
@FunctionalInterface
interface A{
			void dis1();					// by default abstract method mandatory to override 
			default void dis2() {			// can override or re-usability 				
					System.out.println("dis2 is a default method part of A interface");
			}
			default void dis3() {		// can override or re-usability
				System.out.println("dis3 is default method part of A interface");
			}
			static void dis4() {								// we can't override 
				System.out.println("dis4 is a static method in A interface");
			}
			//void dis5();
}
class B implements A {
	@Override
	public void dis1() {
		System.out.println("dis1 method body provided by B class");
	}
	@Override
	public void dis3() {
		System.out.println(" B class override dis3() default  method.");
	}
}
public class Java8Interface {

	public static void main(String[] args) {
		B obj = new B();
		obj.dis1();
		obj.dis2();
		obj.dis3();
		//obj.dis4();
		A.dis4(); // calling static method part of A interface. 
	}

}
