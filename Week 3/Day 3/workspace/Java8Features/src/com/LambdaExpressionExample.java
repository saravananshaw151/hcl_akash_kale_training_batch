package com;
@FunctionalInterface
interface Operation {
	public int add(int x, int y);
}
interface Operation1 {
	public String findLargest(int a,  int b);
}
public class LambdaExpressionExample {
	public static void main(String[] args) {
		Operation op1 = (int x, int y)->x+y;				// by default return 
		System.out.println(op1.add(10, 20));
		Operation op2 = (int a, int b)->a+b;
		System.out.println(op2.add(1, 2));
		Operation op3 = (a,b)->a+b;
		System.out.println(op3.add(100, 200));
		Operation op4= (x,y)-> {
				int sum = x+y;
				return sum;
		};
		System.out.println(op4.add(1000, 2000));
		Operation1 op5= (a,b)-> {
			if(a>b) {
					return "a is largest";
			}else {
				return "b is largest";
			}
		};
		System.out.println(op5.findLargest(1000, 200));
	}

}
