package com;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class BufferedInputAndOutputOperation {

	public static void main(String[] args) throws Exception{
		FileInputStream fis = new FileInputStream("C:\\test.txt");// read
		BufferedInputStream bis = new BufferedInputStream(fis);
		FileOutputStream fos = new FileOutputStream("sample2.txt"); //write 
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		int ch;						
		while((ch=bis.read()) != -1) {			// -1 is equal to EOF (end of the file)
						bos.write(ch);
		}
		bos.flush();      // send the data from buffer memory to file 
		fis.close();
		fos.close();
		System.out.println("File copied...");
	}

	}


