package com;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ByteWiseReadAndWriteFileOperation {

	public static void main(String[] args) throws Exception{
		FileInputStream fis = new FileInputStream("C:\\test.txt");// read
		FileOutputStream fos = new FileOutputStream("sample1.txt"); //write 
		int ch;						
		while((ch=fis.read()) != -1) {			// -1 is equal to EOF (end of the file)
						fos.write(ch);
		}
		fis.close();
		fos.close();
		System.out.println("File copied...");
	}

}
