package com;

import java.io.FileWriter;
import java.io.InputStreamReader;

public class CharacterWiseReadValueAndStoreInFile {

	public static void main(String[] args) throws Exception{
		InputStreamReader isr = new InputStreamReader(System.in);
		FileWriter fw = new FileWriter("info.txt");
		int ch;
		System.out.println("Enter the data");
		while((ch= isr.read()) != '\n') {
			fw.write(ch);
		}
		isr.close();
		fw.close();
	}

}
