package com;

import java.io.DataInputStream;
import java.io.FileOutputStream;

public class SourceKeyboardDestinationFileOrConsole {
	public static void main(String[] args) throws Exception{
		DataInputStream dis = new DataInputStream(System.in);
		//FileOutputStream fos = new FileOutputStream("abc.txt");		// old data will lose add new data 
		FileOutputStream fos = new FileOutputStream("abc.txt",true);	// new data will append	
		System.out.println("Enter the data");
		int ch;
		while((ch=dis.read()) != '\n') {			// till hit the enter key 
			System.out.print(ch);					// display in console but in unicode (asci code)
			System.out.println((char)ch);	// converted in char format. 
			fos.write(ch);								// storing in file in file automatically converstion will happen. 
		}
		fos.close();
	}

}
