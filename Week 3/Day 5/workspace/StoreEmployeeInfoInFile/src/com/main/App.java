package com.main;

import com.bean.Employee;
import com.service.EmployeeService;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EmployeeService es = new EmployeeService();
		
		// store the object 
		//Employee emp = new Employee(100, "Raj", 12000);
		//es.addEmployee(emp);
		
		
		// retreive the object 
		Employee ee = es.getEmployee();
		System.out.println(ee);
	}

}
