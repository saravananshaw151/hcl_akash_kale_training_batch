package com.greatlearning.assignment.bean;

import java.time.LocalDate;

public class Employee {
private int id;
private String name;
private float salary;
private LocalDate dbo;

public Employee() {
	super();
	// TODO Auto-generated constructor stub
}

public Employee(int id, String name, float salary, LocalDate dbo) {
	super();
	this.id = id;
	this.name = name;
	this.salary = salary;
	this.dbo = dbo;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public float getSalary() {
	return salary;
}
public void setSalary(float salary) {
	this.salary = salary;
}
public LocalDate getDbo() {
	return dbo;
}
public void setDbo(LocalDate dbo) {
	this.dbo = dbo;
}
@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + ", dbo=" + dbo + "]";
}

}
