package com.greatlearning.assignment.main;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import com.greatlearning.assignment.bean.Employee;
import com.greatlearning.assignment.service.EmployeeService;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String con;
		int ch;
		EmployeeService es = new EmployeeService();
		int id;
		String name;
		float salary;
		String dob;
		do {
			System.out.println("1:Add \t 2 :Display\t 3: Add More Employee \t 4 : Delete Employee Id ");
			System.out.println("Enter your choice");
			ch = sc.nextInt();
			switch(ch) {
			case 1:		System.out.println("Enter the Id");
			                id = sc.nextInt();
			                System.out.println("Enter the name");
			                name = sc.next();
			                System.out.println("Enter the salary");
			                salary = sc.nextFloat();
			                System.out.println("Enter the Date of Birth in the form of yyyy-mm-dd");
			                dob = sc.next();
							Employee emp = new Employee(id, name, salary,LocalDate.parse(dob,DateTimeFormatter.ofPattern("yyyy-MM-dd")));	
							String res = es.addEmployee(emp);
							System.out.println(res);
							break;
			case 2:     es.getAllEmployeeDetails().forEach(e->System.out.println(e));
							break;
			case 3:		System.out.println("How many records do you want to store");
			                int n= sc.nextInt();
			                for(int i=0;i<n;i++) {
			                	
			                	System.out.println("Enter the Id");
				                id = sc.nextInt();
				                System.out.println("Enter the name");
				                name = sc.next();
				                System.out.println("Enter the salary");
				                salary = sc.nextFloat();
				                System.out.println("Enter the Date of Birth in the form of yyyy-mm-dd");
				                dob = sc.next();
								Employee employee = new Employee(id, name, salary,LocalDate.parse(dob,DateTimeFormatter.ofPattern("yyyy-MM-dd")));	
								String res1 = es.addEmployee(employee);
								System.out.println(res1);
								
			                }
							break;
			case 4:	System.out.println("Enter the id to delete the records");
						id = sc.nextInt();
			            String res2 = es.deleteEmployeeInfo(id);
			            System.out.println(res2);
			            break;
			default :	System.out.println("Wrong choice");
			}
			System.out.println("Do you want to continue?");
			con = sc.next();
		} while (con.equalsIgnoreCase("y"));
		System.out.println("Finish Thank You!");
	}

}
