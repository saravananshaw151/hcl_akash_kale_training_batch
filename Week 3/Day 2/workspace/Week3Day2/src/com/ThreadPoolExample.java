package com;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
class Sample implements Runnable{
	public void run() {
		Thread t = Thread.currentThread();
		String name = t.getName();
		try {
			System.out.println("Start task "+name);
			Thread.sleep(500);
			System.out.println("End task"+name);
		}catch(Exception e) {}
	}
}
public class ThreadPoolExample {
	public static void main(String[] args) {
	Sample ss = new Sample();
		//ExecutorService es= Executors.newFixedThreadPool(10);			// need pool to create 10 thread. fixed 
	ExecutorService es= Executors.newCachedThreadPool();						// run time we can create n number thread. 	
		for(int i=0;i<15;i++ ) {
			es.execute(ss);					//it is like a start thread has to execute. 
		}
//		es.execute(ss);
//		es.execute(ss);
		es.shutdown();
	}

}
