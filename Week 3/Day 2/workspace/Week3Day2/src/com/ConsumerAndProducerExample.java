package com;
class Task {
	    int n;
	    boolean result = false;
		public synchronized void getData() {
			try {
				if (!result) {
					wait();		// if first time call make the thread to wait	and same thread come 2nd time we make get thread to wait. 
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println("Get Data "+n);// value set as 0 through put method 
			result = false;
			notify();						// resume for put thread 
		}
		public synchronized void putData(int i) {
					try {
						if(result) {
							wait();									// 2nd time if same thread come 
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					n = i;					// 1st time value of i set to n as 0
					result = true;
					System.out.println("Data Put "+n);	// 0 value set 
					notify();			// we make getData thread to resume 
		}
}
class Consumer implements Runnable{
		Task tt;					// reference 
		public Consumer(Task tt) {
			this.tt = tt;   // referece of Task class object 
			Thread t = new Thread(this);			// current object 
			t.start();
		}
		public void run() {
			int i=0;
			while(i<=10) {
				tt.getData();											// may be this method work first 
				i++;
			}
		}
}
class Producer implements Runnable{
		Task tt;				// referece of Task class object 
		public Producer(Task tt) {
			this.tt = tt;
			Thread t = new Thread(this);			// inside  a constructor this mean current object. 
			t.start();
		}
		public void run() {
			int i=0;
			while(i<=10) {
				tt.putData(i);								// may be this method work first  this method can call 10 times
				i++;
			}
		}
}
public class ConsumerAndProducerExample {
	public static void main(String[] args) {
		Task tt = new Task();
		Consumer cc = new Consumer(tt);			//pass same class object to consumer class 
		Producer pp = new Producer(tt);				// pass same class object to producer class 
	}

}
