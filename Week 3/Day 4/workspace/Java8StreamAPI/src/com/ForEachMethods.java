package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
//class MyConsumer implements Consumer<String>{
//	@Override
//	public void accept(String t) {
//	System.out.println(" name is "+t);	
//	}
//}
public class ForEachMethods {
		public static void main(String[] args) {
		List<String> names = new ArrayList<>();
		names.add("Raj"); names.add("Ramesh"); names.add("Ajay");
		names.add("Vijay"); names.add("Dinesh"); names.add("Lokesh");
//		Iterator<String> li = names.iterator();
//		System.out.println("Before Java 8 Display names one by one using iterator");
//		while(li.hasNext()) {
//			String name = li.next();
//					System.out.println(name);
//		}
//		System.out.println("After Java 8 Display names one by one using forEach method");
//		Consumer<String> cc = new MyConsumer();			// run time polymorphism 
//		names.forEach(cc);
		System.out.println("After Java 8 Display names using Lambda Expression");
		//names.forEach((v)->System.out.println(v));
		names.forEach((String v)->System.out.println(v));
	}

}
