package com;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StreamAPIExample2 {

	public static void main(String[] args) {
		List<Integer> ll = new ArrayList<>();
		ll.add(10); ll.add(11); ll.add(3); ll.add(4); ll.add(8); ll.add(9); ll.add(1); ll.add(8);
		ll.add(30); ll.add(15); ll.add(3); ll.add(17); ll.add(14); ll.add(19); ll.add(9); ll.add(7);
		// display all numter one by one 
		//ll.stream().forEach(v->System.out.println(v));
		// display only even number 
		//ll.stream().filter(v->v%2==0).forEach(v->System.out.println(v));
		// display only odd number 
		//ll.stream().filter(v->v%2!=0).forEach(v->System.out.println(v));
		// sum of all numbers 
		//int sumOfNumbers = ll.stream().mapToInt(v->v).sum();
		//System.out.println("Sum of all number are "+sumOfNumbers);
		
		int maxNumber = ll.stream().max(Comparator.comparing(v->v)).get();
		System.out.println("Max number is "+maxNumber);
		
		int minNumber = ll.stream().min(Comparator.comparing(v->v)).get();
		System.out.println("Min number is "+minNumber);
	}

}
