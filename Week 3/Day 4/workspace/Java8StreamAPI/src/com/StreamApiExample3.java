package com;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamApiExample3 {

	public static void main(String[] args) {
		List<Employee> listOfEmp = new ArrayList<>();
		listOfEmp.add(new Employee(2, "Raj", 12000));
		listOfEmp.add(new Employee(1, "Ravi", 15000));
		listOfEmp.add(new Employee(3, "Ram", 16000));
		listOfEmp.add(new Employee(6, "Ajay", 8000));
		listOfEmp.add(new Employee(8, "Vijay", 10000));
		listOfEmp.add(new Employee(5, "Lokesh", 18000));
		// using Stream API display all employee details 
		//listOfEmp.stream().forEach(e->System.out.println(e));		// call toString method
		
		// using Stream API display all employee details 
		//listOfEmp.stream().forEach(e->System.out.println("Name is "+e.getName()+" Salary "+e.getSalary()));			
		
		// apply the condition on salary and display the name
		//listOfEmp.stream().filter(e->e.getSalary()>15000).forEach(e->System.out.println(e));
		
		// apply the condition on salary and find the nuber of employee 
//		long numbeOfEmp = listOfEmp.stream().filter(e->e.getSalary()>14000).count();
//		System.out.println("Number of Employee are "+numbeOfEmp);
		
		//Sorted employee base upon asc id wise 
		//listOfEmp.stream().sorted((emp1,emp2)->emp1.getId()-emp2.getId()).forEach(v->System.out.println(v));
		//Sorted employee base upon desc id wise 
		//listOfEmp.stream().sorted((emp1,emp2)->emp2.getId()-emp1.getId()).forEach(v->System.out.println(v));
		
		//Sorted employee base upon asc name wise 
		//listOfEmp.stream().sorted((emp1,emp2)->emp1.getName().compareTo(emp2.getName())).forEach(v->System.out.println(v));
		
		// after apply one or more intermediate operation condition we can store those result in another collection 
		
		List<Employee> sortedNameList = 
		listOfEmp.stream().sorted((emp1,emp2)->emp1.getName().compareTo(emp2.getName())).collect(Collectors.toList());
		
		sortedNameList.forEach(emp->System.out.println(emp.getName()));
	}

}
