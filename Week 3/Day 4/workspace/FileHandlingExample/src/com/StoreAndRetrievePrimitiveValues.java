package com;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class StoreAndRetrievePrimitiveValues {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
//		FileOutputStream fos = new FileOutputStream("emp.txt");
//		DataOutputStream dos = new DataOutputStream(fos);
//		dos.writeInt(100);
//		dos.writeUTF("Ramesh Kumar");
//		dos.writeFloat(12000.00f);
//		System.out.println("Store primitive value");
		
		FileInputStream fis = new FileInputStream("emp.txt");
		DataInputStream dis  = new DataInputStream(fis);
		int id = dis.readInt();
		String name = dis.readUTF();
		float salary = dis.readFloat();
		System.out.println(" id is "+id);
		System.out.println("name "+name);
		System.out.println("salary "+salary);
		fis.close();
		dis.close();
	}

}
