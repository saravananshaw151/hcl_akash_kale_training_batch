package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class ArrayListRetrieve {
	public static void main(String[] args) {
	ArrayList al = new ArrayList();
	al.add(10); 		// auto-boxing 
	al.add(20);
	al.add(30);
	al.add(40);
	System.out.println(al);    // it display as string 
	System.out.println("using Enhanced loop");
	for(Object a :al) {
		System.out.println(a);
	}
	System.out.println("Using iterator");
	  Iterator ii= al.iterator();		// iterator is a method and method return type is Iterator interface reference. 
	  while(ii.hasNext()) {			// it elements is present hasnext method return true 
		  Object obj = ii.next();		// this method retrieve the element and point to next element. 
		  System.out.println(obj);
	  }
	  System.out.println("Using ListIterator - Forward direction");
	  ListIterator li = al.listIterator();		//listIterator is a method and return type if ListIreator interface reference. 
	  while(li.hasNext()) {
		  Object obj = li.next();
		  System.out.println(obj);
	  }
	  System.out.println("Using ListIterator - backward direction ");
	  while(li.hasPrevious()) {
		  Object obj = li.previous();
		  System.out.println(obj);
	  }
	}

}
