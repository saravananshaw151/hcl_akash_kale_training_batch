package com;

import java.util.ArrayList;

public class ArrayListDemo {
	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		al.add(10);		// auto-boxing : it convert primitive to object. 
		int a=20;			//primitive value consider. a is primitive 
		Integer b = new Integer(a);			// b is object 
		int c = b.intValue();				// c is primitive 
		al.add(b);		
		al.add(10.20);
		al.add(true);
		al.add("Ramesh");
		System.out.println(al);
	}

}
