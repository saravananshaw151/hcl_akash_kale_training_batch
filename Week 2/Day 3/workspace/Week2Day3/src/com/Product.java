package com;

public class Product implements Comparable<Product>{
private int pid;
private String pname;
private float price;

public Product() {
	super();
	// TODO Auto-generated constructor stub
}
public Product(int pid, String pname, float price) {
	super();
	this.pid = pid;
	this.pname = pname;
	this.price = price;
}

@Override
public int compareTo(Product o) {
	// TODO Auto-generated method stub
	// if two product id or name or price are equal it return 0 if first > second return +ve or -ve 
	//return this.pid-o.pid;			//can be zero or +ve or -ve pid asc 
	//return o.pid -this.pid;			// pid desc order 
	//return this.pname.compareTo(o.pname); 				//  pname asc order this check both object name asci code 
	//return o.pname.compareTo(this.pname); 				//  pname desc order
	//return (int)(this.price-o.price);								// price asc order  
	return (int)(o.price-this.price);								// price desc order  
}
public int getPid() {
	return pid;
}
public void setPid(int pid) {
	this.pid = pid;
}
public String getPname() {
	return pname;
}
public void setPname(String pname) {
	this.pname = pname;
}
public float getPrice() {
	return price;
}
public void setPrice(float price) {
	this.price = price;
}
@Override
public String toString() {
	return "Product [pid=" + pid + ", pname=" + pname + ", price=" + price + "]";
}

}
