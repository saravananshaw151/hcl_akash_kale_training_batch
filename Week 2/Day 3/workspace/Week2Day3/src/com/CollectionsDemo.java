package com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionsDemo {

	public static void main(String[] args) {
		List<String> ll = new ArrayList<>();
		ll.add("Vijay");
		ll.add("Ajay");
		ll.add("Mahesh");
		ll.add("Kishor");
		ll.add("Dinesh");
		System.out.println("Before sort");
		for(String str:ll) {
			System.out.print(str+" ");
		}
		Collections.sort(ll);					// sort by default Asc 
		System.out.println();
		System.out.println("After sort - Asc");
		for(String str:ll) {
			System.out.print(str+" ");
		}
		Collections.reverse(ll);			// sort desc using reverse 
		System.out.println();
		System.out.println("After sort - Desc");
		for(String str:ll) {
			System.out.print(str+" ");
		}
	}

}
