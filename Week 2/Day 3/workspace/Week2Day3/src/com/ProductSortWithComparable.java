package com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductSortWithComparable {
		public static void main(String[] args) {
			List<Product> listOfProduct = new ArrayList<>();
			listOfProduct.add(new Product(2, "Tv", 56000));
			listOfProduct.add(new Product(1, "Laptop", 85000));
			listOfProduct.add(new Product(3, "Computer", 42000));
			System.out.println("Display Product Before sort");
			for(Product p : listOfProduct) {
				System.out.println(p);    
			}
			Collections.sort(listOfProduct);
			System.out.println();
			System.out.println("Display Product After sort");
			for(Product p : listOfProduct) {
				System.out.println(p);    
			}
		}
}
