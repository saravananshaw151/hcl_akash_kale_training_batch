package com;

import java.util.HashSet;

public class HashSetDemo {

	public static void main(String[] args) {
		HashSet hs = new HashSet();
		System.out.println("Size "+hs.size());
		System.out.println("Is Empty "+hs.isEmpty());
		hs.add(2);
		hs.add(4);
		hs.add(1);
		hs.add(5);
		hs.add(6);
		System.out.println(hs);
		hs.add(4);		// it does't add 
		System.out.println(hs);
		System.out.println("Size "+hs.size());
		System.out.println("Is Empty "+hs.isEmpty());
		System.out.println("Remove "+hs.remove(2));
		System.out.println("Remove "+hs.remove(200));
		System.out.println(hs);
		System.out.println("Search "+hs.contains(4));		// search operation 
		System.out.println("Search "+hs.contains(400));
		hs.clear();
		System.out.println("Size "+hs.size());
	}

}
