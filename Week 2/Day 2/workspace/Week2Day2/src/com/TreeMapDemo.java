package com;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class TreeMapDemo {

	public static void main(String[] args) {
//		TreeMap tm = new TreeMap();
//		tm.put(2, "Raj");
//		tm.put(1, "Ajay");
//		tm.put(3, "Vijay");
//		tm.put(4,"Ramesh");
//		//tm.put("A","Dinesh");
//		System.out.println(tm);
		LinkedHashMap hm = new LinkedHashMap();
		//HashMap hm = new HashMap();
		//TreeMap hm = new TreeMap();
		hm.put(null, "Raj");
		hm.put(null, "Raju");
		hm.put(2, null);
		hm.put(3, null);
		System.out.println(hm);
	}

}
