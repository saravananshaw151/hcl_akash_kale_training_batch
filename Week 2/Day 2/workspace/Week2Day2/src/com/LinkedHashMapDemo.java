package com;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class LinkedHashMapDemo {

	public static void main(String[] args) {
		//HashMap mm = new HashMap();
		LinkedHashMap mm = new LinkedHashMap();
		
		mm.put(3,"Ramesh");
		mm.put(1, "Ajay");
		mm.put(4, "Balaji");
		mm.put("A", "Dinesh");
		mm.put(2, "Vijay");
		System.out.println(mm);
	}

}
