package com;

import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		TreeSet ts = new TreeSet();
		ts.add(3);
		ts.add(1);
		ts.add(5);
		ts.add(4);
		ts.add(6);
		ts.add(7);
		//ts.add("Raj");
		System.out.println(ts);
		System.out.println(ts.tailSet(4));// from 4 value to till end 
		System.out.println(ts.headSet(4));	// from begining to 4th value. 
		System.out.println(ts.subSet(3, 6));		// from start to end 
	}

}
