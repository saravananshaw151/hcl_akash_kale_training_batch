package com;

public class VoteEligibleException extends Exception{

	public VoteEligibleException(String msg) {
		super(msg);			// super class parameter constructor call. we will set the message. 		
	}
	public VoteEligibleException() {
		super();			// super class Exception class constructor 
	}
}
