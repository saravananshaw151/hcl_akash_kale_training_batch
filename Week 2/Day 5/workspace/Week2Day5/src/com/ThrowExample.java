package com;

public class ThrowExample {

	public static void main(String[] args) {
		int age=16;
		try {
			if(age<21) {
				//throw new Exception();			// Generic Generated 
				//throw new ArithmeticException(); // Specific Generated
				//int a=10/0;
				//throw new ArithmeticException("Age must be > 21"); // Speicific exception with custom message. 
				//throw new VoteEligibleException();	// Custom Exception 
				throw new VoteEligibleException("Age must be > 21");  // custom exception with custom messgae.
			}else {
				System.out.println("You Can Vote!");
			}
		}catch(Exception e) {
			System.out.println(e);
		}
	}

}
