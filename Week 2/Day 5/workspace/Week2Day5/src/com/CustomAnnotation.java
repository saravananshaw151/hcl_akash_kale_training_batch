package com;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
@interface Sample {				// class leval as well as method level 
	
}
@Target(ElementType.METHOD)
@interface Demo {
	
}
@Target(ElementType.METHOD)
@interface Emp {
	String name();   
}
@Sample()
class Operation {
	@Sample
	void dis1() {
		
	}
	@Demo()
	void dis2() {
		
	}
	@Emp(name = "Raj Deep")
	void dis3() {
		
	}
}
public class CustomAnnotation {

	public static void main(String[] args) {

	}

}
