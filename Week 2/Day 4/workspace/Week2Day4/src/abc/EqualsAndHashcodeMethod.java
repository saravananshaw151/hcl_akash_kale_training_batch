package abc;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class EqualsAndHashcodeMethod {

	public static void main(String[] args) {
		String str1 = "Raj";
		String str2 = "Raj";
		System.out.println(str1.equals(str2));
		Integer i = 10;
		Integer j=10;
		System.out.println(i.equals(j));
		Customer c1 = new Customer(1, "Raj", 21);
		Customer c2 = new Customer(1, "Raj", 21);
		
		Customer c3 = new Customer(1, "Ravi", 21);
		Customer c4 = new Customer(2, "Ravi", 21);
		System.out.println(" "+c1.hashCode());
		System.out.println(" "+c2.hashCode());
		System.out.println(" "+c3.hashCode());
		System.out.println(" "+c4.hashCode());
		System.out.println(c1.equals(c2));			// internally equals method Object class.
		System.out.println(c3.equals(c4));
		Set<Customer> ss = new HashSet<>();
		ss.add(c1);
		ss.add(c2);
		ss.add(c3);
		ss.add(c4);
		System.out.println("Number of object are "+ss.size());
	}

}
