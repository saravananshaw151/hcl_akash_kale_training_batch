package abc;

public class Customer {
private int custId;
private String custName;
private int age;

//@Override
//public boolean equals(Object obj) {
//		return true;
//}

public Customer() {
	super();
	// TODO Auto-generated constructor stub
}




@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + age;
	result = prime * result + custId;
	result = prime * result + ((custName == null) ? 0 : custName.hashCode());
	return result;
}




@Override
public boolean equals(Object obj) {
	Customer cc = (Customer)obj;
if(this.custId==cc.custId && this.custName.equals(cc.getCustName()) && this.age==cc.age) {
	return true;
}else {
	return false;
}
}




public Customer(int custId, String custName, int age) {
	super();
	this.custId = custId;
	this.custName = custName;
	this.age = age;
}
public int getCustId() {
	return custId;
}
public void setCustId(int custId) {
	this.custId = custId;
}
public String getCustName() {
	return custName;
}
public void setCustName(String custName) {
	this.custName = custName;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
@Override
public String toString() {
	return "Customer [custId=" + custId + ", custName=" + custName + ", age=" + age + "]";
}

}
