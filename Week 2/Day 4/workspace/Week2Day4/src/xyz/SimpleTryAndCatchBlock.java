package xyz;

public class SimpleTryAndCatchBlock {
	public static void main(String[] args) {
		System.out.println("Hi");
		int a=10;
		int b=1;
		int abc[]= {10,20,30,40,50};
		try {
		int res = a/b;
		System.out.println("Result "+res);
		int res1 = 100/abc[4];
		System.out.println("Result "+res1);
		}catch (Exception e) {
			//System.out.println("I Take Care");
			System.out.println(e);    // internally call toString method. 
		}
		System.out.println("Bye....");
		System.out.println("Bye....");
		System.out.println("Bye....");
	}

}
