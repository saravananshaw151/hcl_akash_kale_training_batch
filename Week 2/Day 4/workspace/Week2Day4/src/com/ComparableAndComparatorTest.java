package com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class ComparableAndComparatorTest {

	public static void main(String[] args) {
		List<Employee> listOfEmp = new ArrayList<>();
		listOfEmp.add(new Employee(2, "Ajay", 140000));
		listOfEmp.add(new Employee(1, "Vijay", 120000));
		listOfEmp.add(new Employee(3, "Mahesh", 160000));
		System.out.println("Before sort");
		for(Employee emp:listOfEmp) {
			System.out.println(emp);
		}
		//Collections.sort(listOfEmp);			// using Comparable 
		Collections.sort(listOfEmp, new SortEmployeeNameAsc());   // using Comparator 
		System.out.println("After sort");
		for(Employee emp:listOfEmp) {
			System.out.println(emp);
		}
	}

}
