async function loadData() {
    let obj = await fetch("http://localhost:3000/emp")
    let res = await obj.json();
    console.log(res);
    
}
loadData();

async function addRecord(){
let empId = document.getElementById("id").value;
let empName = document.getElementById("name").value;
let empAge = document.getElementById("age").value;
let obj = await fetch("http://localhost:3000/emp",{
    method:"post",
    body:JSON.stringify({
        id:empId,
        name:empName,
        age:empAge
    }),
    headers:{
        "Content-type":"application/json"
    }
});
let res = await obj.json();
console.log(res);
loadData();
}