package mno;
class Bike {
	void speed() {
		System.out.println("60km/hr");
	}
}
class Honda extends Bike {				// re-usability of speed method 
	void color() {
		System.out.println("Red");
	}
}
class Tvs extends Bike {
	void color() {
		System.out.println("White");
	}
	void speed() {
		super.speed();					// we are calling super class speed method 	merge code 
		System.out.println("20km/hr");
	}
}
class Pulsar extends Bike{
		void color() {
			System.out.println("Black");
		}
		void speed() {											// override 
			System.out.println("90km/hr");
		}
}
public class MethodOverriding {
	public static void main(String[] args) {
		Honda hh  = new Honda();		hh.speed();
		Pulsar pu  = new Pulsar(); 		pu.speed();
		Tvs tv = new Tvs();				tv.speed();
	}

}
