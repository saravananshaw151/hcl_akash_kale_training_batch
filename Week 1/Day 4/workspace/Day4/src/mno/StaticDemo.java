package mno;
class Abc {
		int a;
		static int b;
		void dis1() {
				System.out.println("non static method");
				System.out.println("a "+a);
				System.out.println("b "+b);
		}
		static void dis2() {
			System.out.println("static method");
			//System.out.println("a "+a);
			System.out.println("b "+b);
		}
}
public class StaticDemo {
	public static void main(String[] args) {
	Abc obj = new Abc();
	obj.a=10;
	Abc.b=20;     // through class name
	obj.b=30;
	obj.dis1();
	obj.dis2();
	Abc.dis2();   // through class name
	}

}
