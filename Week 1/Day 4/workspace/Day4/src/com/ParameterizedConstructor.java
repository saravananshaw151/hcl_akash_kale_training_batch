package com;
class Calculation {
	int a,b,sum;
	Calculation() {
		a=10;
		b=20;
	}
	Calculation(int a, int b){		//parameter or local variable 
	//	a=a;		// local variable = local variable 
	//	b=b;
		this.a = a;		// instance variable = local variable 
		this.b = b;
	}
	void setValue(int a, int b) {
		this.a =a;
		this.b =b;
	}
	void add() {
		sum = a+b;
	}
	void display() {
		System.out.println("sum is "+sum);
	}
}
public class ParameterizedConstructor {
	public static void main(String[] args) {
	Calculation c1 = new Calculation();   c1.display();		// 0
	Calculation c2  = new Calculation();  c2.add();    c2.display();  // 30
	Calculation c3  = new Calculation(); c3.add();		c3.display();  // 30
	Calculation c4 = new Calculation(100, 200); c4.add();  c4.display();  // 300
	Calculation c5 = new Calculation();
	c5.setValue(1, 2);
	c5.setValue(3, 4);
	c5.setValue(6, 7);
	c5.add();
	c5.display();
	}

}
