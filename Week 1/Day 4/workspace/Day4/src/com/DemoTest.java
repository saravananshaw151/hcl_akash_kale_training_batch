package com;

import java.util.Random;
class Abc {

	void displayInfo() {
		System.out.println("This is display Info method");
	}
	
	void add(int x, int y) {
		int sum = x+y;
		System.out.println("Sum of two number is "+sum);
	}
	
	String sayHello(String name) {
		return "Welcome user "+name;
	}
	
	int getNumber() {
				Random rr = new Random();
				int num=	rr.nextInt();		// this method generate the unique number. 
				return num;
	}
}
public class DemoTest {

	public static void main(String[] args) {
		Abc obj = new Abc();
		obj.displayInfo();
		obj.add(10, 20);
		obj.add(1, 2);
		String result =obj.sayHello("Raj");
		System.out.println(obj.sayHello("Ramesh"));
		System.out.println(result);
		int res = obj.getNumber();
		System.out.println(res);
	}

}
