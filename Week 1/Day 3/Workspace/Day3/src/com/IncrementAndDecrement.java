package com;

public class IncrementAndDecrement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a=10;
		int b=a;
		System.out.println(b);		//a =10 . b =10
		b=++a;			// pre-increment 
		System.out.println(b);		//a=11, b = 11
		b=a++;			// post increment 
		System.out.println(b);		//a=12, b = 11
	}

}
