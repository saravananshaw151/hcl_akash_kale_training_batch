package com;

public class StringDemo {

	public static void main(String[] args) {
//		String str1="Welcome to Java Training";
//		String str2 = new String("Welcome to Java Training");
//		System.out.println(str1);
//		System.out.println(str2);
//		System.out.println(str2.length());
//		System.out.println(str1.toUpperCase());
//		System.out.println(str1.toLowerCase());
//		System.out.println(str1.substring(4));
		String str1 = "Raj";
		String str2 = "Raj";
		String str3 = new String("Raj");
		String str4 = new String("raj");
		if(str3.equalsIgnoreCase(str4)) {
			System.out.println("Equal");
		}else {
			System.out.println("Not Equal");
		}
	}

}
