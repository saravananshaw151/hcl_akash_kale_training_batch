package com;
class Abc {
	Abc() {
		this("Raj");
		System.out.println("empty constructor");
	}
	Abc(int x){
		System.out.println("one parameter");
	}
	Abc(String name){
		this(10);
		System.out.println("two parameter");
	}
}
public class ThisParameterExample {
	public static void main(String[] args) {
		Abc obj1 = new Abc();
		//Abc obj2 = new Abc(10);
		//Abc obj3  =new Abc("Raj");
	}

}
