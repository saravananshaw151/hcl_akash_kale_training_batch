package com;
class A {
		int n=10;
}
class B extends A {
		int n=20;
		void dis1() {
			int n=30;
			System.out.println(" n local variable "+n);		// local 
			System.out.println("n instance variable "+this.n); // instance 
			System.out.println("n super class variable "+super.n); // super class variable
		}
}
public class SuperAndThisKeyword {
	public static void main(String[] args) {
	B obj = new B();
	obj.dis1();
	}

}
