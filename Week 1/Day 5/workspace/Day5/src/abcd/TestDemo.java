package abcd;
class Employee {
		int id;
		String name;
		float salary;
		Employee() {
			this.id = 123;
			this.name="Unknown";
			this.salary = 8000;
		}
		Employee(int id, String name){
			this();
			this.id = id;
			this.name=name;
		}
		Employee(int id,String name,float salary) {
					this.id = id;
					this.name = name;
					this.salary = salary;
		}
}
class Manager extends Employee {
	int numberOfEmp;
		Manager(int id,String name, float salary, int numberOfEmp) {
			super(id,name,salary);
			this.numberOfEmp=numberOfEmp;
		}
		Manager(int id, String name, int numberOfEmp){
			super(id,name);
			this.numberOfEmp=numberOfEmp;
		}
		Manager() {
			super();
			this.numberOfEmp=5;
		}
}
public class TestDemo {
	public static void main(String[] args) {
		Manager mgr = new Manager();
		Manager mgr1 = new Manager(100,"Raj",45000,20);
		Manager mgr2 = new Manager(101,"Ramesh",15);
	}

}
