package abc;

public class Employee {
	private int a=10;	//private 
	int b=20;					// default 
	protected int c=30;	//protected 
	public int d=40;			//public 
	public void disEmpInfo() {
		System.out.println("a "+a);
		System.out.println("b "+b);
		System.out.println("c "+c);
		System.out.println("d "+d);
	}
}
