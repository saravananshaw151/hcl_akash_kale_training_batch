package com;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration			// like beans.xml 
@ComponentScan(basePackages = "com") // <component:scan/>
public class MyConfiguration {

}
