package com;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BeanFactory factory = new ClassPathXmlApplicationContext("beans.xml");
		Employee emp1 =(Employee)factory.getBean("emp1"); 
		emp1.display();
		System.out.println(emp1);
		emp1.setId(100);
		emp1.setName("Raju");
		emp1.setSalary(12000);
		System.out.println(emp1);
		Employee emp2 = (Employee)factory.getBean("emp1");
		System.out.println(emp2);
		emp2.setId(200);
//		System.out.println(emp1);
//		System.out.println(emp2);
		
		Employee emp3 = (Employee)factory.getBean("emp2");
		emp3.setId(101);
		emp3.setName("Ramesh");
		emp3.setSalary(14000);
		System.out.println(emp3);
		Employee emp4 = (Employee)factory.getBean("emp2");
		System.out.println(emp4);
		
		
		Employee emp5 = (Employee)factory.getBean("emp3");
		System.out.println(emp5);
		
		Address add1 = (Address)factory.getBean("add1");
		System.out.println(add1);
		
		Address add2 = (Address)factory.getBean("add2");
		System.out.println(add2);
		
		
		Employee employee = (Employee)factory.getBean("employee");
		System.out.println(employee);
	}

}
