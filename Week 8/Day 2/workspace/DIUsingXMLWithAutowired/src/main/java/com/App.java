package com;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BeanFactory factory = new ClassPathXmlApplicationContext("beans.xml");
		Employee emp = (Employee)factory.getBean("employee");
		System.out.println(emp);
	}

}
