package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Employee;
import com.dao.EmployeeDao;

@Service						// id employeService 
public class EmployeeService {

	@Autowired							
	EmployeeDao employeeDao;			// DI for Dao layer 
	
	public String storeEmployeeData(Employee emp) {
		if(emp.getSalary()<12000) {
				return "Salary must be > 12000";
		}else {
				if(employeeDao.storeEmployeeInfo(emp)>0) {
					return "Record stored successfully";
				}else {
					return "Record didn't store";
				}
		}
	}
	
	public List<Employee> getAllEmployee() {
		return employeeDao.retrieveEmployeeInfo();
	}
}
