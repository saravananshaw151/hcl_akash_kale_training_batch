package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Employee;
import com.service.EmployeeService;

@Controller
public class EmployeeController {

	
	@Autowired
	EmployeeService employeeService;
	
	@RequestMapping(value = "employeeDetails",method = RequestMethod.GET)
	public ModelAndView getAllEmployeeInfo(HttpSession hs) {			// DI for Session Object 
		List<Employee> listOfEmp = employeeService.getAllEmployee();
		hs.setAttribute("obj", listOfEmp);												// Store the object in session scope. 
		ModelAndView mav = new ModelAndView();
		mav.setViewName("displayEmployee.jsp");
		return mav;
	}
	<input type="hidden" name="bookid" value=<%=bb.getBookId()%></br/>
	@RequestMapping(value = "deleteEmployee",method = RequestMethod.GET)
	public ModelAndView deleteEmploye(HttpServletRequest req,HttpSession hs) {			// DI for Session Object 
		int id = Integer.parseInt(req.getParameter("id"));
		String res = employeeService.deleteRecord(id);
		List<Employee> listOfEmp = employeeService.getAllEmployee();
		hs.setAttribute("obj", listOfEmp);		
		hs.setAttribute("deleteMsg", res);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("displayEmployee.jsp");
		return mav;
	}
	
	@RequestMapping(value = "updateEmployee",method = RequestMethod.GET)
	public ModelAndView updateEmploye(HttpServletRequest req,HttpSession hs) {			// DI for Session Object 
		int id = Integer.parseInt(req.getParameter("id"));
		hs.setAttribute("id", id);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("updateRecord.jsp");
		return mav;
	}
	
	
}
