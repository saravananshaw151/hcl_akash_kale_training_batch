package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Employee;

@RestController 				// @RestController = @Controller + @ResponseBody
public class EmployeeController {

	// http://localhost:8080/SpringRestFullWebService/sayHi
	@RequestMapping(value = "sayHi",method = RequestMethod.GET)
	public String sayHi() {
		return "Welcome to Simple Rest Controller Message";
	}
	// http://localhost:8080/SpringRestFullWebService/html
	@RequestMapping(value = "html",method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
	public String sayHtml() {
		return "<h2>This is Simple Message</h2>";
	}
	// http://localhost:8080/SpringRestFullWebService/xml
	@RequestMapping(value = "xml",method = RequestMethod.GET,produces = MediaType.TEXT_XML_VALUE)
	public String sayPlainText() {
		return "<Abc>This is Simple Message</Abc>";
	}
	// http://localhost:8080/SpringRestFullWebService/text
	@RequestMapping(value = "text",method = RequestMethod.GET,produces = MediaType.TEXT_PLAIN_VALUE)
	public String sayXml() {
		return "<h2>This is Simple Message<h2>";
	}
	// Single query param concept 
	// http://localhost:8080/SpringRestFullWebService/singleQueryParam?name=Raj
	@RequestMapping(value = "singleQueryParam",method = RequestMethod.GET)
	public String passSingleValue(@RequestParam("name") String fname) {
		return "Welcome user "+fname;
	}
	// Multiple query param concept 
	// http://localhost:8080/SpringRestFullWebService/multipleQueryParam?name=Raj&pass=123
	// http://localhost:8080/SpringRestFullWebService/multipleQueryParam?name=Ravi&pass=123
	@RequestMapping(value = "multipleQueryParam",method = RequestMethod.GET)
	public String passMultiValue(@RequestParam("name") String name, @RequestParam("pass") String pass) {
			if(name.equals("Ravi") && pass.equals("123")) {
				return "Successfully login";
			}else {
				return "Failure try once again";
			}
	}
	
	//single path param 
	// http://localhost:8080/SpringRestFullWebService/singlePathValue/Ravi
	@RequestMapping(value = "singlePathValue/{name}")
	public String singlePathValue(@PathVariable("name") String name) {
			return "Welcome user to path param "+name;
	}
	
		//multi path param 
		// http://localhost:8080/SpringRestFullWebService/multiPathValue/Ravi/123
		@RequestMapping(value = "multiPathValue/{name}/{pass}")
		public String multiPathValue(@PathVariable("name") String name, @PathVariable("pass") String pass) {
				if(name.equals("Raj") && pass.endsWith("123")) {
					return "success";
				}else {
					return "failure";
				}
		}
		
		// http://localhost:8080/SpringRestFullWebService/getEmployee 
		@RequestMapping(value = "getEmployee",
				method = RequestMethod.GET,
				produces = MediaType.APPLICATION_JSON_VALUE)
		public Employee getEmployeeInfo() {
			Employee emp = new Employee(100,"Ravi",12000);
			return emp;
		}
		// http://localhost:8080/SpringRestFullWebService/getAllEmployee 
				@RequestMapping(value = "getAllEmployee",
						method = RequestMethod.GET,
						produces = MediaType.APPLICATION_JSON_VALUE)
				public List<Employee> getAllEmployeeInfo() {
					Employee emp1 = new Employee(100,"Ravi",12000);
					Employee emp2 = new Employee(101,"Ramesh",15000);
					Employee emp3 = new Employee(102,"Rajesh",18000);
					Employee emp4 = new Employee(103,"Raju",20000);
					List<Employee> listOfemp = new ArrayList<>();
					listOfemp.add(emp1);
					listOfemp.add(emp2);
					listOfemp.add(emp3);
					listOfemp.add(emp4);
					// List<Employee> listOfEmp = employeeServie.getAllEmployee();
					return listOfemp;
				}
				
				
				// http://localhost:8080/SpringRestFullWebService/storeEmployee
				// data {"id":100,"name":"Ramesh","salary":12000}
				
				@RequestMapping(value = "storeEmployee",
						method = RequestMethod.POST,
						consumes = MediaType.APPLICATION_JSON_VALUE,
						produces = MediaType.TEXT_PLAIN_VALUE)
				public String storeEmployeeDetails(@RequestBody Employee emp) {
					// do the logic. 
					// we can call Service layer. 
					System.out.println(emp);
					return "Employee Stored";
				}	
				
				
				@RequestMapping(value = "updateEmployee",
						method = RequestMethod.PUT,
						consumes = MediaType.APPLICATION_JSON_VALUE,
						produces = MediaType.TEXT_PLAIN_VALUE)
				public String updateEmployeeDetails(@RequestBody Employee emp) {
					// do the logic. 
					// we can call Service layer. 
					System.out.println(emp);
					return "Employee Record updated";
				}
				
				@RequestMapping(value = "deleteEmployeeInfo/{id}",method = RequestMethod.DELETE)
				public String deleteEmployeeInfo(@PathVariable("id") int empId) {
					// write the code to delete the records 
					return "Record deleted successfully with id as "+empId;
				}
				
}
