package com;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {
//		Employee emp  = new Employee();
//		emp.display();
		
		// Spring Pre-Defined API 
		
		BeanFactory factory = new ClassPathXmlApplicationContext("beans.xml");
		Employee emp=(Employee)factory.getBean("emp1");
		emp.display();
	}

}
