// function style object creation 
function Employee() {
    //id = 100;           // local variable or private variable 
    this.id = 100;
    this.name = 'Raj';
    this.age = 21;
    this.dis = function() {
        document.write("<br> dis function part of employee object")
    }
}
//Employee();
var emp1 = new Employee();
document.write("Function style access object property")
document.write("<br/> Id is "+emp1.id);
document.write("<br/> Name is "+emp1.name);
document.write("<br/> Age is "+emp1.age);
emp1.dis();

// literal style object creation 
let emp2 = {id:100,name:"Raj Deep",age:21};
document.write("<br/>Literal style object creation")
document.write("<br/> Id is "+emp2.id);
document.write("<br/> Name is "+emp2.name);
document.write("<br/> Age is "+emp2.age);
