//ES6 style 
function loadInfo() {
let obj = fetch("https://fakestoreapi.com/products")
obj.then(response=>response.json()).then(data=>console.log(data)).catch(error=>console.log("Catch block "+error))
}

loadInfo();

//ES7 style 
async function loadInfoDetails() {
    try{
    let obj = await fetch("https://fakestoreapi.com/products"); // asynchronous 
    let res = await obj.json();
    console.log(res);
    }catch(Ex){
        console.log(Ex)
    }
}
loadInfoDetails();