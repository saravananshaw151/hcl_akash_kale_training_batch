// let emp = {id:100,name:"Ravi",age:21,skillSet:["C","C++","Java"],address:{city:"Bangalore",state:"Kar"}};
// document.write("Object creation in literal style<br/>")
// document.write("<br/> Id is "+emp.id);
// document.write("<br/> Name is "+emp.name);
// document.write("<br/> Age is "+emp.age);
// document.write("<br/> Skill "+emp.skillSet)
// document.write("<br/> Skill "+emp.skillSet[0])
// document.write("<br/> city "+emp.address.city);
// document.write("<br/> Age is "+emp.address.state);
// // convert object literal/json to string format. 
// let empString = JSON.stringify(emp);
// document.write("<br/> Id is "+empString.id);
// document.write("<br/> Name is "+empString.name);
// document.write("<br/> Age is "+empString.age);
// // convert string to json format. 
// let empJson = JSON.parse(empString);
// document.write("<br/> Id is "+empJson.id);
// document.write("<br/> Name is "+empJson.name);
// document.write("<br/> Age is "+empJson.age);
// document.write("<br/> Skill "+empJson.skillSet)
// document.write("<br/> Skill "+empJson.skillSet[0])
// document.write("<br/> city "+empJson.address.city);
// document.write("<br/> Age is "+empJson.address.state);

//let emp = {"id":100,"name":"Raj"}     // object literal 
let emp ='{"id":100,"name":"Raj"}';     // string 
JSON.parse(emp);

