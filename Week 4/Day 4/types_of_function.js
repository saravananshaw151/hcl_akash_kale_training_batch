// // normal function 
// function display1() {
//     document.write("<br/> This is normal function")
// }
// display1();
// // Expression style function with name
// let display2 = function display() {
//     document.write("<br/>This is expression style function with name")
// }
// // we can't call function using display() now 
// // the function is varible name consider. 
// display2();
// //Expression style function with anonymous s name
// let display3 = function() {
//     document.write("<br/> This is expression style function with anonymous name")
// }
// display3();
// // Arrow function 
// let display4 = ()=>document.write("<br/>This is simple arrow function");
// display4();

// // Expression style passing two parameter 
// let addNumber1 = function(a,b){
//     var sum = a+b;
//     return sum;
// }
// document.write("<br/> Sum of two number is "+addNumber1(10,20));

// // Arrow style passing two parameter, arrow function by default return without return keyword. 
// let addNumber2 = (a,b)=>a+b;
// document.write("<br/> Sum of two number is  "+addNumber2(10,20));


function greeting(fname,callback){
    return "Welcome "+callback(fname);
}
// Normal style function 
function maleInfo(fname) {
    return "Mr "+fname;
}
// Expression style function 
let femaleInfo = function(fname){
    return "Miss "+fname;
}
document.write(greeting("Raj",maleInfo))
document.write("<br/>")
document.write(greeting("Seeta",femaleInfo))
document.write("<br/>")
document.write(greeting("Ajay",function(fname){
    return "Mr "+fname;
}));
document.write("<br/>")
document.write(greeting("Meeta",(fname)=>"Miss "+fname));
