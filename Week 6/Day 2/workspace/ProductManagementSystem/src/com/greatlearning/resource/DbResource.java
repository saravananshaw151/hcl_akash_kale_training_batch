package com.greatlearning.resource;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbResource {
	private static Connection con;
	private DbResource() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root@123");
		} catch (Exception e) {
			System.out.println("Db Connection error "+e);
		}
	}
	public static Connection getDbConnection() {
		try {
			return con;
		} catch (Exception e) {}
		return null;
	}
	
	// This method from main call at the last once all operation work finish. 
	public static void closeConnection() {
		try {
			con.close();
		} catch (Exception e) {}
	}
}
