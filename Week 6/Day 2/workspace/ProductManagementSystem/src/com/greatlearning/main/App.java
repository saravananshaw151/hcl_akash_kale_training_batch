package com.greatlearning.main;

import com.greatlearning.bean.Product;
import com.greatlearning.service.ProductService;

public class App {

	public static void main(String[] args) {
		ProductService ps = new ProductService();
		
		// get all product details with 10% discount
		//ps.findProduct().forEach(p->System.out.println(p));
		
		// find product using pid
//		String res = ps.findProduct(102);
//		System.out.println(res);
		
		// delete product 
//		String res = ps.deleteProduct(100);
//		System.out.println(res);
		
		//update product price 
//		Product pr = new Product();
//		pr.setPid(101);
//		pr.setPrice(8500);
//		String res = ps.updateProduct(pr);
//		System.out.println(res);
		
		
		// insert Record 
		Product pr = new Product();
		pr.setPid(100);
		pr.setPname("Tv");
		pr.setPrice(120000);
		String res = ps.storeProductDetails(pr);
		System.out.println(res);
	}

}
