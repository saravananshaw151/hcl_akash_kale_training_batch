package com.greatlearning.service;

import java.util.Iterator;
import java.util.List;

import com.greatlearning.bean.Product;
import com.greatlearning.dao.ProductDao;

public class ProductService {

	
	public String storeProductDetails(Product prod) {
		if(prod.getPrice()>1000) {
				ProductDao pd = new ProductDao();
				if(pd.storeProductInfo(prod)>0) {
					return "Record stored successfully";
				}else {
					return "Record didn't store";
				}
		}else {
			return "Product price must be > 1000";
		}
	}
	
	public String updateProduct(Product prod) {
				ProductDao pd = new ProductDao();
				float oldPrice = pd.findProductPrice(prod.getPid());
				if(oldPrice==0) {
					return "Product not present";
				}else if(oldPrice>prod.getPrice()) {
						return "Price must be > old price";
				}else if(pd.udateProduct(prod)>0){
						return "Product price updated successfully";
				}else {
						return "Product didn't update";
				}
	}
	
	public String deleteProduct(int pid) {
							ProductDao pd = new ProductDao();
							if(pd.deleteProduct(pid)>0) {
								return "Record deleted successfully";
							}else {
								return "Record didn' delete";
							}
	}
	
	public String findProduct(int pid) {
			ProductDao pd  = new ProductDao();
			Product p = pd.findProduct(pid);
			if(p==null) {
				return "Product not present";
			}else {
					return p.toString();
			}
	}
	
	public List<Product> findProduct() {
		ProductDao pd  = new ProductDao();
		List<Product> listOfProduct = pd.findAllProduct();
		Iterator<Product> li = listOfProduct.iterator();
		while(li.hasNext()) {
			Product p = li.next();
			p.setPrice(p.getPrice()-p.getPrice()*0.10f);
		}
		
		return listOfProduct;
	}
	
}
