package com;
interface Document {
	public void create();
}
class Notepad implements Document{
	public void create() {
		System.out.println("Notepad document created");
	}
}
class MsWord implements Document{
	public void create() {
		System.out.println("MsWord document created");
	}
}
class Pdf implements Document{
	public void create() {
		System.out.println("Pdf document created");
	}
}
class DocumentFactory {
	public static Document getInstance(String type) {
		if(type.equalsIgnoreCase("notepad")) {
			return new Notepad();
		}else if(type.equalsIgnoreCase("msword")) {
			return new MsWord();
		}else if(type.equalsIgnoreCase("pdf")) {
			return new Pdf();
		}else {
			return null;
		}
	}
}
public class FactoryMethodDesignPattern {
	public static void main(String[] args) {
		Document dd1 = DocumentFactory.getInstance("notepad");
		dd1.create();
		Document dd2 = DocumentFactory.getInstance("msword");
		dd2.create();
		Document dd3 = DocumentFactory.getInstance("pdf");
		dd3.create();
	}

}
