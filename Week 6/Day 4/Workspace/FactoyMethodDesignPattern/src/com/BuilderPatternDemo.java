package com;
class User {
			// immutable property : don't allow to change the value ie don't provide the setter methods and make property are final.
			private final int uid;
			private final String fname;
			private final String lname;
			private final int age;
			private final long phonenumber;
			private final String city;
			public int getUid() {
				return uid;
			}
			public String getFname() {
				return fname;
			}
			public String getLname() {
				return lname;
			}
			public int getAge() {
				return age;
			}
			public long getPhonenumber() {
				return phonenumber;
			}
			public String getCity() {
				return city;
			}
			private User(UserBuilder userBuilder) {			//
					this.uid= userBuilder.uid;
					this.fname=userBuilder.fname;
					this.lname=userBuilder.lname;
					this.age= userBuilder.age;
					this.phonenumber= userBuilder.phonenumber;
					this.city= userBuilder.city;
			}
			
			@Override
			public String toString() {
				return "User [uid=" + uid + ", fname=" + fname + ", lname=" + lname + ", age=" + age + ", phonenumber="
						+ phonenumber + ", city=" + city + "]";
			}

			// static inner class which help to build the object 
			public static class UserBuilder {
				private final int uid;								// mandatory 
				private final String fname;					// mandatory while building the object. 
				private String lname;
				private int age;
				private long phonenumber;
				private String city;
				public UserBuilder(int uid, String fname) {		// parameterized constructor with final inner class property 
					this.uid=uid;
					this.fname=fname;
				}
				public UserBuilder lname(String lname) {
					this.lname= lname;
					return this;
				}
				public UserBuilder age(int age) {
					this.age= age;
					return this;
				}
				public UserBuilder phone(long phonenumber) {
					this.phonenumber=phonenumber;
					return this;
				}
				public UserBuilder city(String city) {
					this.city= city;
					return this;
				}
				public User build() {							// this method responsible to create the object. 
					User user = new User(this);			// this keyword is UserBuilder class object. it call outer class parameterized 
					return user;									// constructor. 
				}
			}
}

public class BuilderPatternDemo {

	public static void main(String[] args) {
		User user1 = new User.UserBuilder(1, "Raj").build();
		System.err.println("Id is "+user1.getUid()+" Name is "+user1.getFname());
		User user2 = new User.UserBuilder(2, "Ram").lname("Patil").build();
		System.err.println("Id is "+user2.getUid()+" Name is "+user2.getFname()+" LName "+user2.getLname());
		User user3 = new User.UserBuilder(3, "Ajay").lname("Kumar").age(21).build();
		User user4 = new User.UserBuilder(4, "Dinesh").age(34).city("Bangalore").build();
	}

}
