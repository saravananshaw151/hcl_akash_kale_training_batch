package com;
interface Strategy {
	void sort(int num[]);
}
class BubbleSort implements Strategy{
	public void sort(int num[]) {
		// coding for bubble sort
		System.out.println("Bubble sort");
	}
}
class InsertionSort implements Strategy{
	public void sort(int num[]) {
		// coding for insertion sort
		System.out.println("Insertion sort");
	}
}
class QuickSort implements Strategy{
	public void sort(int num[]) {
		// coding for quick sort
		System.out.println("Quick sort");
	}
}
class MergeSort implements Strategy{
	public void sort(int num[]) {
		// coding for merge sort
		System.out.println("Merge sort");
	}
}
class Context {
	private Strategy ss;
	public Context(Strategy ss) {
		// TODO Auto-generated constructor stub
		this.ss = ss;
	}
	public void arrange(int num[]) {
		ss.sort(num);
	}
}

public class StrategyDesignPattern {
	public static void main(String[] args) {
		int abc[]= {3,1,5,2,6,9,7};
		Context sort = new Context(new InsertionSort());
		sort.arrange(abc);
	}

}
