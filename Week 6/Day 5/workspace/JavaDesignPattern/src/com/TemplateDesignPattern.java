package com;
abstract class Game {
	abstract void toss();								// must be provide the body 
	abstract void startGame();
	void interval() {										// if you want you can override. 
		System.out.println("30min");
	}
	abstract void endGame();
	abstract void result();
	public final void play() {					// you can't override. 
			toss();
			startGame();
			interval();
			endGame();
			result();
	}
}
class Cricket extends Game {

	@Override
	void toss() {
		// TODO Auto-generated method stub		
		System.out.println("Cricket game toss done!");
	}

	@Override
	void startGame() {
		// TODO Auto-generated method stub
		System.out.println("Cricket game started!");
	}

	@Override
	void endGame() {
		// TODO Auto-generated method stub
		System.out.println("Cricket game end!");
	}

	@Override
	void result() {
		// TODO Auto-generated method stub
		System.out.println("Cricket game Result :");
	}
	
}
class Football extends Game {

	@Override
	void toss() {
		// TODO Auto-generated method stub
		System.out.println("Football game toss done!");
	}

	@Override
	void startGame() {
		// TODO Auto-generated method stub
		System.out.println("Football game start!");
	}

	@Override
	void endGame() {
		// TODO Auto-generated method stub
		System.out.println("Football game end!");
	}

	@Override
	void result() {
		// TODO Auto-generated method stub
		System.out.println("Football game toss done!");
	}
	
}
public class TemplateDesignPattern {
	public static void main(String[] args) {
		Game g1 = new Football();
		g1.play();
	}

}
