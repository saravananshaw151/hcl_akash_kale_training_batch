package com;
interface MediaPlayer {
	public void play();
	public void pause();
	public void stop();
}
class MediaPlayerFacade implements MediaPlayer {
	FileLoader fl = new FileLoader();
	Decoder dd = new Decoder();
	Sound ss = new Sound();
	public void play() {
				fl.loadFile();
				dd.decode();
				ss.on();
	}		
	public void pause() {
		ss.off();
	}
	public void stop() {
		ss.off();
		fl.unloadFile();
	}
}
class FileLoader {
		public void loadFile() {
			System.out.println(" File is loading ....");
		}
		public void unloadFile() {
			System.out.println("Unloading file");
		}
}
class Decoder {
	public void decode() {
		System.out.println(" Decoding file");
	}
}
class Sound {
	public void on() {
		System.out.println(" Sound is coming");
	}
	public void off() {
		System.out.println("It is silent now...");
	}
}
public class FacadeDesignPattern {

	public static void main(String[] args) {
		MediaPlayer mm = new MediaPlayerFacade();
		mm.play();
			mm.pause();
			mm.stop();
	}

}









