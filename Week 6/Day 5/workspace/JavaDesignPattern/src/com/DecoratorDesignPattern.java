package com;
abstract class Sandwich {
	protected String description = "Sandwich";
	public String getDescription() {
		return description;
	}
	abstract double price();
}
class WhiteBreadSandwich extends Sandwich {
	public WhiteBreadSandwich(String type) {
			this.description = type+" "+this.description;
	}
	public double price() {
		return 90;
	}
}
class BrownBreadSandwich extends Sandwich {
	public BrownBreadSandwich(String type) {
			this.description = type+" "+this.description;
	}
	public double price() {
		return 120;
	}
}
// Generic decorator with basic features. 
abstract class SandwichDecorator extends Sandwich {
	
}

class CheeseDecorator extends SandwichDecorator {
	Sandwich ss;
	public CheeseDecorator(Sandwich ss) {
		this.ss= ss;
	}
	@Override
	public String getDescription() {
		return ss.getDescription()+" With Cheese";
	}
	public double price() {
		return ss.price()+50;
	}
}
class PannerDecorator extends SandwichDecorator {
	Sandwich ss;
	public PannerDecorator(Sandwich ss) {
		this.ss= ss;
	}
	@Override
	public String getDescription() {
		return ss.getDescription()+" With Panner";
	}
	public double price() {
		return ss.price()+70;
	}
}

public class DecoratorDesignPattern {
	public static void main(String[] args) {
		Sandwich mySandwich1 = new WhiteBreadSandwich("White bread");
		Sandwich mySandwich2 = new BrownBreadSandwich("Brown bread");
		Sandwich mySandwich3 = new CheeseDecorator(mySandwich2);
		Sandwich mySandwich4 = new PannerDecorator(mySandwich2);
		Sandwich mySandwich5 = new PannerDecorator(mySandwich3);
		System.out.println("Price "+mySandwich1.price()+", "+mySandwich1.getDescription());
		System.out.println("Price "+mySandwich2.price()+", "+mySandwich2.getDescription());
		System.out.println("Price "+mySandwich3.price()+", "+mySandwich3.getDescription());
		System.out.println("Price "+mySandwich4.price()+", "+mySandwich4.getDescription());
		System.out.println("Price "+mySandwich5.price()+", "+mySandwich5.getDescription());
	}
}
