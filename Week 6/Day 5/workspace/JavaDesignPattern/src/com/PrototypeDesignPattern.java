package com;
class Employee  implements Cloneable{
	int id;
	String name;
	float salary;
	public Employee() {
		
	}
	Employee(int id, String name, float salary){
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	public Employee getClone() {
							try {
								Object obj = clone();
								Employee emp = (Employee)obj;
								return emp;
							} catch (Exception e) {
								System.out.println(e);
							}
							return null;
	}
}
public class PrototypeDesignPattern {
	public static void main(String[] args) {
	Employee emp1 = new Employee(100, "Raj", 12000);					// new memory created. 
	Employee emp2 = new Employee(101, "Ramesh",14000);				//new memory created..
	Employee emp3 = emp1;				// same memory 2 reference emp1 and emp3			
	System.out.println(emp1);
	System.out.println(emp2);
	System.out.println(emp3);
	emp3.salary=24000;						// emp1 and emp3 point to same memory if we do any changes through emp3 it will effect to emp1 and vice-versa. 
	System.out.println(emp1);
	System.out.println(emp2);
	System.out.println(emp3);
	Employee emp4 = emp1.getClone();
	System.out.println("After clone created");
	System.out.println(emp1);
	System.out.println(emp4);
	emp4.salary=35000;
	System.out.println("After done changes on clone object");
	System.out.println(emp1);
	System.out.println(emp4);
	}

}
