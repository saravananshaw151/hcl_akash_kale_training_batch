package com;
class Customer {
		String name;
		private Customer() {}
		public void display() {
			System.out.println("Name "+name);
		}
		private static Customer cc = new Customer();
		public static Customer getInstance() {
			return cc;
		}
}
public class SignletonDesignPattern {

	public static void main(String[] args) {
		Customer c1 = Customer.getInstance();
		c1.name="Raj";
		Customer c2 = Customer.getInstance();
		//c2.name="Ravi";
		c1.display();			
		c2.display();
		//Customer c3 = Customer.cc;
	}

}
