package test;

import static org.junit.Assert.*;		// static import 
import static java.lang.Math.*;		// we can call all static method directly
import org.junit.Test;

import com.Operation;

public class OperationTest {

	@Test
	public void testAdd() {
		Operation op = new Operation();
		int result	 = op.add(10, 20);
		assertEquals(30, result);
	}
	@Test
	public void testSub() {
		Operation op  = new Operation();
		int result = op.sub(100, 50);
		assertEquals(50, result);
	}
}
