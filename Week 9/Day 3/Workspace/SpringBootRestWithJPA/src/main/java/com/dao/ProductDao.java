package com.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bean.Product;

@Repository
public class ProductDao {

	@Autowired
	EntityManagerFactory emf;				// it is equal to SessionFactory in Hibernate 
	
	public int storeProductInfo(Product pro) {
		try {
			EntityManager manager = emf.createEntityManager();		// like creating session object in hibernate 
			EntityTransaction tran = manager.getTransaction();				// like Transaction reference in hibernate 
			tran.begin();
					manager.persist(pro);													// like session.save(object) in hibernat 
			tran.commit();
			return 1;
		} catch (Exception e) {
			System.out.println(e);
			return 0;
		}
	}
	
	public List<Product> getAllProduct() {
		EntityManager manager = emf.createEntityManager();
		Query qry = manager.createQuery("select p from Product p");		// product is a entity class name case sensitive 
		List<Product> listOfProduct = qry.getResultList();
		return listOfProduct;
	}
	
	
	public Product findProductById(int pid) {
		EntityManager manager = emf.createEntityManager();
		Product pro = manager.find(Product.class, pid);
		if(pro==null) {
			return null;
		}else {
			return pro;
		}
	}
	
	public int updateProduct(Product p) {
		EntityManager manager = emf.createEntityManager();
		Product pro = manager.find(Product.class, p.getPid());
		EntityTransaction tran = manager.getTransaction();		
		if(pro==null) {
			return 0;
		}else {
				tran.begin();
				pro.setPrice(p.getPrice());
				manager.merge(pro);
				tran.commit();
				return 1;
		}
	}

	public int deleteProduct(int pid) {
		EntityManager manager = emf.createEntityManager();
		Product pro = manager.find(Product.class, pid);
		EntityTransaction tran = manager.getTransaction();		
		if(pro==null) {
			return 0;
		}else {
				tran.begin();
				manager.remove(pro);
				tran.commit();
				return 1;
		}
	}
}
