package com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")		//root path is product 
public class ProductController {

	@GetMapping(value = "getProduct")
	public String getProductInfo() {
		return "Welcome to Product Details";
	}
}
