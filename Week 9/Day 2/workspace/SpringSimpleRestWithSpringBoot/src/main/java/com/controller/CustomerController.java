package com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")				// main path or root path 
public class CustomerController {

	@GetMapping(value = "getCustomer")	// @RequestMapping(value="sayCustomer",method=RequestedMethod.GET)
	public String getCustomerInfo() {
		return "Welcome to Customer controller";
	}
	
}
