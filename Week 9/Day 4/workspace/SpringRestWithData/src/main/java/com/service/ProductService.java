package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Product;
import com.dao.ProductDao;

@Service
public class ProductService {

	@Autowired
	ProductDao productDao;
	
	public List<Product> getAllProducts() {
		return productDao.findAll();
	}
	
	public String storeProductInfo(Product prod) {
				
						if(productDao.existsById(prod.getPid())) {
									return "Product id must be unique";
						}else {
									productDao.save(prod);
									return "Product stored successfully";
						}
	}
	
	public String deleteProductInfo(int pid) {
		if(!productDao.existsById(pid)) {
			return "Product details not present";
			}else {
			productDao.deleteById(pid);
			return "Product deleted successfully";
			}	
	}
	
	public String updateProductInfo(Product prod) {
		if(!productDao.existsById(prod.getPid())) {
			return "Product details not present";
			}else {
			Product p	= productDao.getById(prod.getPid());	// if product not present it will give exception 
			p.setPrice(prod.getPrice());						// existing product price change 
			productDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "Product updated successfully";
			}	
	}
	
}
