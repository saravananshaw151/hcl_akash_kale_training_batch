package com;

import org.springframework.stereotype.Service;

@Service
public class MyService {

	public void simpleServiceMethod() {
		System.out.println("This is simple service method");
	}
}
