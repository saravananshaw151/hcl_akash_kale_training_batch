package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App implements CommandLineRunner{

	@Autowired
	MyService myService;
	
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);		// This code call run the method 
		System.out.println("Spring boot application is running...");
	}

	@Override
	public void run(String... args) throws Exception {
		// ready to do some task. 
		System.out.println("Run method running...");
		myService.simpleServiceMethod();
	}

	
}
