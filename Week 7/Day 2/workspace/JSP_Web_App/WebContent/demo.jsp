<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Welcome to Simple HTML Web Page</h2>
<%!int a,b,sum,sub; %>
<%
	//int a;				// it consider as local variable
	//int b;
	//int sum = a+b;
	//int sub = a-b;
	a=10;
	b=20;
	sum = a+b;
	sub = a-b;
	//out.println("Welcome to JSP Web Page");
	//out.println("<br/>sum of two number is "+sum);
	//out.println("<br/>Sub of two number is "+sub);
%>
<br/>
<p>Sum of two number is <%=10+20%></p>
<br/>
<p>Sum of two number is <%=sum %></p>
<font color="red">Sum of <%=a %> and <%=b %> is <%=sum %></font>
</body>
</html>