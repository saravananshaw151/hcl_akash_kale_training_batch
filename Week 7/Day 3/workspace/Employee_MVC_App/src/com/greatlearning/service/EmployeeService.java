package com.greatlearning.service;

import java.util.List;
import java.util.stream.Collectors;

import com.greatleaning.bean.Employee;
import com.greatlearning.dao.EmployeeDao;

public class EmployeeService {

	
	public String storeEmployee(Employee emp) {
		if(emp.getSalary()<12000) {
			return "Salary is must be > 12000";
		}else {
			EmployeeDao ed = new EmployeeDao();
			if(ed.storeEmployee(emp)>0) {
				return "Record stored successfully";
			}else {
				return "Record didn't store";
			}
		}
	}
	
	public List<Employee> getAllEmployee() {
		EmployeeDao ed = new EmployeeDao();
		return ed.getAllEmployee();
		//return ed.getAllEmployee().stream().mapToDouble(emp->emp.setSalary(1000)).collect(Collectors.toList());
	}
}
