package com.greatlearning.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.greatlearning.bean.Trainer;
import com.greatlearning.resource.SessionFactoryResource;

public class TrainerDao {

	public int storeTrainerInfo(Trainer trainer) {
		try {
			SessionFactory sf = SessionFactoryResource.getSessionFactoryInfo();
			Session session = sf.openSession();
			Transaction tran = session.getTransaction();
			tran.begin();
					session.save(trainer);
			tran.commit();
					return 1;
		} catch (Exception e) {
			System.out.println(e);
			return 0;
		}
	}
}
