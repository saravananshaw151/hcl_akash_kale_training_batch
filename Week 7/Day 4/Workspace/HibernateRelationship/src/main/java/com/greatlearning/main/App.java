package com.greatlearning.main;

import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.Student;
import com.greatlearning.bean.Trainer;
import com.greatlearning.dao.StudentDao;
import com.greatlearning.dao.TrainerDao;

public class App {

	public static void main(String[] args) {
		// Store Trainer details 
//		Trainer t1 = new Trainer();
//		t1.setTid(2);
//		t1.setTname("Ravi");
//		t1.setTech("Python");
//		TrainerDao td = new TrainerDao();
//		int res = td.storeTrainerInfo(t1);
//		if(res==0) {
//			System.out.println("Trainer record didn't store");
//		}else {
//			System.out.println("Trainer record stored successfully");
//		}
		// Store Student Records 
//		Student s1 = new Student();
//		s1.setSid(103);
//		s1.setSname("Veeta");
//		s1.setAge(25);
//		s1.setTsid(2);
//		StudentDao sd = new StudentDao();
//		int res = sd.storeStudentInfo(s1);
//		if(res==0) {
//			System.out.println("Record didn't store");
//		}else {
//			System.out.println("Student Record stored..");
//		}
		// Through Trainer object we are inserting more than one Student records. 
		Trainer t3 = new Trainer();
		t3.setTid(3);
		t3.setTname("Ramesh");
		t3.setTech("Angular");
		List<Student> listOfStd = new ArrayList<>();
		Student s5 = new Student();
		s5.setSid(104);
		s5.setSname("Leeta");
		s5.setAge(32);
		s5.setTsid(3);
		Student s6 = new Student();
		s6.setSid(105);
		s6.setSname("Ueeta");
		s6.setAge(34);
		s6.setTsid(3);
		
		listOfStd.add(s5);
		listOfStd.add(s6);
		
		t3.setListOfStd(listOfStd);
		TrainerDao td = new TrainerDao();
		int res = td.storeTrainerInfo(t3);
		if(res==0) {
			System.out.println("Record didn't store");
		}else {
			System.out.println("Record stored..");
		}
	}

}
