package com.greatlearning.resource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryResource {

	public static SessionFactory getSessionFactoryInfo() {
		Configuration con = new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory sf = con.buildSessionFactory();
		return sf;
	}
}
