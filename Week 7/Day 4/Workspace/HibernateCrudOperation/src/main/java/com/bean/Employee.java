package com.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity			// This annotation must be part of javax.persitence package. 
@Table(name="employee")	// this annotation we can use if table name and class may be different
public class Employee {
@Id										// @Id annotation of that property which is primary key.
private int id;
@Column(name="name")		// this annotation we cann use if variable and column name may be different
private String name;
private float salary;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public float getSalary() {
	return salary;
}
public void setSalary(float salary) {
	this.salary = salary;
}
@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
}

}
