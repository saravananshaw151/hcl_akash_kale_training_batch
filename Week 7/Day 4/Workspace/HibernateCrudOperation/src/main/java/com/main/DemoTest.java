package com.main;

import java.util.List;

import com.bean.Employee;
import com.dao.EmployeeDao;

public class DemoTest {

	public static void main(String[] args) {
	// Insert Query 
//	Employee emp = new Employee();
//	emp.setId(8);
//	emp.setName("Lokesh");
//	emp.setSalary(18000);
//	EmployeeDao ed = new EmployeeDao();
//	int res = ed.storeEmployeeRecord(emp);
//	if(res==0) {
//		System.out.println("Record didn't insert");
//	}else {
//		System.out.println("Record insert successfully");
//	}
	
		
	// Update Record 
		
//		Employee emp = new Employee();
//		emp.setId(9);
//		emp.setSalary(22000);
//		EmployeeDao ed = new EmployeeDao();
//		int res = ed.updateEmployeeRecord(emp);
//		if(res==0) {
//			System.out.println("Record didn't update");
//		}else {
//			System.out.println("Record updated successfully");
//		}
		
		// Delete record 
//		EmployeeDao ed = new EmployeeDao();
//		int res = ed.deleteEmployeeRecord(100);
//		if(res==0) {
//			System.out.println("Record not present");
//		}else {
//			System.out.println("Record deleted");
//		}
		
		// Retrieve employee record using employee id 
//		EmployeeDao ed = new EmployeeDao();
//		Employee e = ed.findRecord(3);
//		if(e==null) {
//			System.out.println("Record not present");
//		}else {
//			System.out.println("id is "+e.getId()+" Name is "+e.getName()+" Salary is "+e.getSalary());
//		}
		
		// Retrieve all records 
//		EmployeeDao ed= new EmployeeDao();
//		List<Employee> listOfEmp = ed.getAllEmployees();
//		listOfEmp.forEach(emp->System.out.println(emp));
		
		
		// Retrieve all records with conditions 
		EmployeeDao ed= new EmployeeDao();
		List<Employee> listOfEmp = ed.getAllEmployeesWithCondition(19000);
		listOfEmp.forEach(emp->System.out.println(emp));
		
	}

}
